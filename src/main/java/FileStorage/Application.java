package FileStorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Created by rahul on 25/01/16.
 */
@SpringBootApplication
//@EnableResourceServer
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    /*
    @Override
    public void configure(HttpSecurity http) throws Exception {

        http
                //apply OAuth protection to only 1 resources
                .requestMatchers().antMatchers("/getFile/{filename:.+}").and()
                .authorizeRequests()
                .anyRequest().permitAll();

    }
    */

}
